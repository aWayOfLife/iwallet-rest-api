const jwt = require('jsonwebtoken')
const RefreshToken = require('./models/RefreshToken')


module.exports = function auth(req,res,next){
    const accessToken  = req.header('auth-token')
    const refreshToken = req.header('refresh-token')

    if(!accessToken) return res.sendStatus(401)

    jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, async(err,user)=>{
        if(err) return await newAccessToken(refreshToken,req,res,next)

        req.data = {accessToken:accessToken,refreshToken:refreshToken,user:user}
        next()
    })
    
}

const newAccessToken = async(refreshToken, req, res, next) =>{

    if(!refreshToken) return res.sendStatus(401)

        const refreshTokenExists = await RefreshToken.findOne({refreshToken:refreshToken})

        if(!refreshTokenExists) return res.sendStatus(401)

        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err,user)=>{
            if(err) return res.sendStatus(401)

            const accessToken = generateAccessToken(user)

            req.data = {accessToken:accessToken,refreshToken:refreshToken,user:user}
            next()
        })
}

const generateAccessToken =(user) =>{
     const token  = jwt.sign({_id:user._id, email:user.email, name:user.name}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:'1m'})
     return token
}

