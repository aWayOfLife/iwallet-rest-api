const mongoose = require('mongoose')

const RefreshTokenSchema = mongoose.Schema({
    refreshToken:{
        type: String,
        required:true,
        max:1024
    }
})

module.exports = mongoose.model('RefreshToken', RefreshTokenSchema)