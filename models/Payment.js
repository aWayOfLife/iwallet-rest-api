const mongoose = require('mongoose')


const PaymentSchema = mongoose.Schema({

    payeeAccountName:{
        type:String,
        required:true
    },
    payeeAccountNumber:{
        type:String,
        required:true
    },
    payeeBankName:{
        type:String,
        required:true        
    },
    payeeBankIFSC:{
        type:String,
        length:11,
        required:true        
    },
    payeeTransactionType:{
        type:String,
        required:true        
    },
    payerCardNumber:{
        type:String,
        length:16,
        required:true        
    },
    payerCardName:{
        type:String,
        required:true        
    },
    payerCardExpiry:{
        type:String,
        length:7,
        required:true        
    },
    payerCardCVV:{
        type:String,
        length:3,
        required:true        
    },
    payerAmount:{
        type:String,
        required:true        
    }
})

module.exports = mongoose.model('Payment', PaymentSchema)