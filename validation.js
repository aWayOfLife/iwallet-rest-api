//Validation
const Joi = require('@hapi/joi');

//Register Validation - wrap it in a function as there will be multiple schemas
const paymentValidation = data => {

const schema = Joi.object({
    payeeAccountName:Joi.string().required(),
    payeeAccountNumber:Joi.string().min(11).max(16).required(),
    payeeBankName:Joi.string().required(),
    payeeBankIFSC:Joi.string().length(11).required(),
    payeeTransactionType:Joi.string().required(),
    payerCardNumber:Joi.string().length(16).required(),
    payerCardName:Joi.string().required(),
    payerCardExpiry:Joi.string().length(7).required(),
    payerCardCVV:Joi.string().length(3).required(),
    payerAmount:Joi.string().min(1).required()
    })
    return schema.validate(data);

}

const registerValidation = data => {

const schema = Joi.object({
    name:Joi.string().min(3).required(),
    email:Joi.string().min(6).required(),
    password:Joi.string().min(6).required()
    })
    return schema.validate(data);

}

const loginValidation = data => {

const schema = Joi.object({
    email:Joi.string().min(6).required(),
    password:Joi.string().min(6).required()
    })
    return schema.validate(data);

}

module.exports.paymentValidation = paymentValidation;
module.exports.loginValidation = loginValidation;
module.exports.registerValidation = registerValidation;
