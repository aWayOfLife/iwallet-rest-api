const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv/config')

const app = express()

//IMPORT ROUTE
const paymentRoute = require('./routes/payment')
const authRoute = require('./routes/auth')

//MIDDLEWARE
app.use(express.json())
app.use(express.urlencoded({
    extended:true
}))
app.use(cors())

app.use('/api/user', authRoute)
app.use('/api/payment', paymentRoute)


mongoose.connect(process.env.DB_CONNECTION,{ useNewUrlParser: true, useFindAndModify:true, useUnifiedTopology:true }, () => console.log("connected to DB"))
app.listen(process.env.PORT ||5000, () => console.log("Server is up"))