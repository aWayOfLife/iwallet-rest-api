const express = require('express')
const Payment = require('../models/Payment')
const router  = express.Router()
const { paymentValidation } = require('../validation');
const verify = require('../verifyToken')


//ROUTES
router.get('/', async (req,res)=>{
    try{
        const payments = await Payment.find()
        res.status(200).send(payments)
    }catch(err){
        res.status(400).send({message:err})
    }
})

router.get('/:paymentId', async (req,res)=>{
    try{
        const payment = await Payment.findById(req.params.paymentId)
        res.status(200).send(payment)
    }catch(err){
        res.status(400).send({message:err})
    }
})

router.post('/',verify, async (req,res) =>{

    //VALIDATE

    const {error} = paymentValidation(req.body)
    if(error){
        return res.status(400).send(error.details[0].message)
    }
    const payment = new Payment(req.body)
    try{
        const savedPayment = await payment.save()
        res.status(200).send({accessToken:req.data.accessToken, refreshToken: req.data.refreshToken, payment:savedPayment})
    }catch(err){
        res.status(400).send(err)
    }
})

module.exports = router