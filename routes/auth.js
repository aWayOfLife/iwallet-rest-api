const express = require('express')
const User = require('../models/User')
const router  = express.Router()
const userModel = require('../models/User')
const { registerValidation, loginValidation } = require('../validation');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const verify = require('../verifyToken');
const { ref } = require('@hapi/joi');
const RefreshToken = require('../models/RefreshToken')


const generateAccessToken =(user) =>{
     const token  = jwt.sign({_id:user._id, email:user.email, name:user.name}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:'1m'})
     return token
}

const generateRefreshToken =(user) =>{
     const token  = jwt.sign({_id:user._id, email:user.email, name:user.name}, process.env.REFRESH_TOKEN_SECRET, {expiresIn:'10m'})
     return token
}

//VALIDATION

router.get('/',verify, async (req,res)=>{
    try{
        console.log(req.user)
        const user = await User.findById(req.data.user._id)
        return res.status(200).send({accessToken:req.data.accessToken, refreshToken:req.data.refreshToken, user:user})
    }catch(err){
        return res.status(400).send({message:err})
    }
})

router.post('/register', async (req,res) =>{

    //VALIDATE

    const {error} = registerValidation(req.body)
    if(error){
        return res.status(400).send(error.details[0].message)
    }

    const emailExist = await User.findOne({email:req.body.email})
    if(emailExist){
        return res.status(400).send('EMAIL ID ALREADY EXISTS')
    }
    
    //HASH
    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(req.body.password, salt)

    const user = new User({
        name:req.body.name,
        email:req.body.email,
        password:hashPassword
    })

    try{
        const savedUser = await user.save()
        //const token  = jwt.sign({_id:savedUser.id, name:savedUser.name}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:'2h'})
        const accessToken  = generateAccessToken(savedUser)
        const refreshToken = generateRefreshToken(savedUser)
        //add refreshtoken to DB
        const token = new RefreshToken({
            refreshToken:refreshToken
        })
        const saveToken = await token.save()

        return res.status(200).send({accessToken:accessToken, refreshToken:refreshToken, user:savedUser})

    }catch(err){
        return res.status(400).send(err)
    }
})



router.post('/login', async(req,res) =>{
    //VALIDATE
    console.log(req.body)
    const {error} = loginValidation(req.body)
    if(error){
        return res.status(400).send(error.details[0].message)
    }

    try{

        const user = await User.findOne({email:req.body.email})
        if(!user){
            return res.status(400).send('EMAIL ID DOES NOT EXIST')
        }
        
        //HASH
        
        const isValid = await bcrypt.compare(req.body.password, user.password)
        if(!isValid){
            return res.status(400).send('INCORRECT PASSWORD')
        }
        
        const accessToken  = generateAccessToken(user)
        const refreshToken = generateRefreshToken(user)
        //add refreshtoken to DB
        const token = new RefreshToken({
            refreshToken:refreshToken
        })
        const saveToken = await token.save()

        return res.status(200).send({accessToken:accessToken, refreshToken:refreshToken, user:user})

    }catch(err){
        return res.status(400).send(err)
    }
    
   
})


router.post('/token', async(req, res) =>{
    const refreshToken = req.header('refresh-token')
    

    //check if refreshtoken exits
    if(!refreshToken) return res.sendStatus(401)

    const refreshTokenExists = await RefreshToken.findOne({refreshToken:refreshToken})
    if(!refreshTokenExists) return res.sendStatus(401)

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err,user)=>{
        if(err) return res.sendStatus(401)

        const accessToken = generateAccessToken(user)
        return res.status(200).send({accessToken:accessToken})
    })

})


router.delete('/logout', async(req,res)=>{
    const refreshToken = req.header('refresh-token')
    if(!refreshToken) return res.sendStatus(401)

    //delete from db
    try{
        const token = await RefreshToken.remove({refreshToken:refreshToken})
        return res.sendStatus(204)
    }catch(error){
        return res.sendStatus(400)
    }


})



module.exports = router